package kz.astana.dialogapplication;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private SimpleDateFormat dateFormat, timeFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Back pressed", Toast.LENGTH_SHORT).show();
            }
        });
        toolbar.getMenu().clear();
        toolbar.inflateMenu(R.menu.my_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.alarm) {
                    Toast.makeText(MainActivity.this, "Alarm pressed", Toast.LENGTH_SHORT).show();
                    return true;
                } else if (item.getItemId() == R.id.archive) {
                    Toast.makeText(MainActivity.this, "Archive pressed", Toast.LENGTH_SHORT).show();
                    return true;
                }
                return false;
            }
        });

        dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

        Button alertDialogWithText = findViewById(R.id.alertDialogWithText);
        alertDialogWithText.setOnClickListener(this);

        Button alertDialogWithList = findViewById(R.id.alertDialogWithList);
        alertDialogWithList.setOnClickListener(this);

        Button alertDialogWithMultiChoice = findViewById(R.id.alertDialogWithMultiChoice);
        alertDialogWithMultiChoice.setOnClickListener(this);

        Button datePickerDialog = findViewById(R.id.datePickerDialog);
        datePickerDialog.setOnClickListener(this);

        Button timePickerDialog = findViewById(R.id.timePickerDialog);
        timePickerDialog.setOnClickListener(this);

        Button progressDialog = findViewById(R.id.progressDialog);
        progressDialog.setOnClickListener(this);

        Button horizontalProgressDialog = findViewById(R.id.horizontalProgressDialog);
        horizontalProgressDialog.setOnClickListener(this);

        Button customDialog = findViewById(R.id.customDialog);
        customDialog.setOnClickListener(this);

        Button firstCustomDialogFragment = findViewById(R.id.firstCustomDialogFragment);
        firstCustomDialogFragment.setOnClickListener(this);

        Button secondCustomDialogFragment = findViewById(R.id.secondCustomDialogFragment);
        secondCustomDialogFragment.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.alertDialogWithText:
                showAlertDialogWithText();
                break;
            case R.id.alertDialogWithList:
                showAlertDialogWithList();
                break;
            case R.id.alertDialogWithMultiChoice:
                showAlertDialogWithMultiChoice();
                break;
            case R.id.datePickerDialog:
                showDatePickerDialog();
                break;
            case R.id.timePickerDialog:
                showTimePickerDialog();
                break;
            case R.id.progressDialog:
                showProgressDialog();
                break;
            case R.id.horizontalProgressDialog:
                showHorizontalProgressDialog();
                break;
            case R.id.customDialog:
                showCustomDialog();
                break;
            case R.id.firstCustomDialogFragment:
                showFirstCustomDialogFragment();
                break;
            case R.id.secondCustomDialogFragment:
                showSecondCustomDialogFragment();
                break;
            default:
                Log.d("Hello", "Default");
        }
    }

    private void showAlertDialogWithText() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Dialog title");
        builder.setMessage("It's alert dialog with this message");
        builder.setIcon(R.drawable.ic_age);
        builder.setCancelable(false);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setNeutralButton("I don't know", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showAlertDialogWithList() {
        String[] countries = {"Kazakhstan", "Russia", "China"};

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Choose your country");
        builder.setItems(countries, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this, countries[which], Toast.LENGTH_SHORT).show();
            }
        });
        builder.setCancelable(false);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showAlertDialogWithMultiChoice() {
        boolean[] checked = {false, false, false};
        String[] countries = {"Kazakhstan", "Russia", "China"};

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Choose your country(-ies)");
        builder.setMultiChoiceItems(
                countries,
                checked,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        checked[which] = isChecked;
                    }
                }
        );
        builder.setCancelable(false);
        builder.setPositiveButton("Show", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                StringBuilder s = new StringBuilder();
                for (int i = 0; i < checked.length; i++) {
                    if (checked[i]) {
                        s.append(countries[i]).append(" is checked\n");
                    } else {
                        s.append(countries[i]).append(" is not checked\n");
                    }
                }

                Toast.makeText(
                        MainActivity.this,
                        s.toString(),
                        Toast.LENGTH_LONG
                ).show();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            datePickerDialog = new DatePickerDialog(MainActivity.this);
            datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(year, month, dayOfMonth);

                    Toast.makeText(
                            MainActivity.this,
                            dateFormat.format(calendar.getTime()),
                            Toast.LENGTH_LONG
                    ).show();
                }
            });
        }

        Calendar c = Calendar.getInstance();
        c.set(2020, 9, 20);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        c.set(2020, 10, 10);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePickerDialog.show();
    }

    private void showTimePickerDialog() {
        Calendar calendar = Calendar.getInstance();

        TimePickerDialog dialog = new TimePickerDialog(
                MainActivity.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(0, 0, 0, hourOfDay, minute);
                        Toast.makeText(
                                MainActivity.this,
                                timeFormat.format(calendar.getTime()),
                                Toast.LENGTH_LONG
                        ).show();
                    }
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true
        );
        dialog.show();
    }

    private void showProgressDialog() {
        ProgressDialog dialog = new ProgressDialog(MainActivity.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Downloading...");
        dialog.show();
    }

    private void showHorizontalProgressDialog() {
        ProgressDialog dialog = new ProgressDialog(MainActivity.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setMessage("Downloading!!!");
        dialog.setMax(200);
        dialog.show();
        dialog.setProgress(45);
    }

    private void showCustomDialog() {
        View view = LayoutInflater
                .from(MainActivity.this)
                .inflate(R.layout.layout_dialog, null);

        ImageView image = view.findViewById(R.id.dialogImageView);
        TextView text = view.findViewById(R.id.dialogTextView);
        Button button = view.findViewById(R.id.dialogButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image.setImageResource(R.drawable.ic_popcorn);
                text.setText("I changed text");
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(view);
        builder.setTitle("Custom dialog");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showFirstCustomDialogFragment() {
        FirstDialogFragment dialog = new FirstDialogFragment();
        dialog.show(getSupportFragmentManager(), "First dialog");
    }

    private void showSecondCustomDialogFragment() {
        SecondDialogFragment dialog = new SecondDialogFragment();
        dialog.show(getSupportFragmentManager(), "Second dialog");
    }
}