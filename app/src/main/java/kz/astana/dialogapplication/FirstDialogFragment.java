package kz.astana.dialogapplication;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class FirstDialogFragment extends DialogFragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().setTitle("First custom dialog");

        View view = inflater.inflate(R.layout.fragment_first_dialog, container, false);

        Button yes = view.findViewById(R.id.yesButton);
        yes.setOnClickListener(this);
        Button maybe = view.findViewById(R.id.maybeButton);
        maybe.setOnClickListener(this);
        Button no = view.findViewById(R.id.noButton);
        no.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.yesButton) {
            Toast.makeText(getContext(), "Yes pressed", Toast.LENGTH_SHORT).show();
        } else if (v.getId() == R.id.maybeButton) {
            Toast.makeText(getContext(), "Maybe pressed", Toast.LENGTH_SHORT).show();
        } else if (v.getId() == R.id.noButton) {
            Toast.makeText(getContext(), "No pressed", Toast.LENGTH_SHORT).show();
        }
        dismiss();
    }
}
