package kz.astana.dialogapplication;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class SecondDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Second custom dialog");
        builder.setMessage("It's your second dialog");
        builder.setPositiveButton("Yes", this);
        builder.setNegativeButton("No", this);
        builder.setNeutralButton("Maybe", this);

        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == Dialog.BUTTON_POSITIVE) {
            Toast.makeText(getContext(), "Yes button pressed", Toast.LENGTH_SHORT).show();
        } else if (which == Dialog.BUTTON_NEGATIVE) {
            Toast.makeText(getContext(), "No button pressed", Toast.LENGTH_SHORT).show();
        } else if (which == Dialog.BUTTON_NEUTRAL) {
            Toast.makeText(getContext(), "Maybe button pressed", Toast.LENGTH_SHORT).show();
        }
    }
}
